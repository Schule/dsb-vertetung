require('dotenv').config();
var _ = require('lodash');
const DsbUntis = require('dsb-untis');
const fs = require("fs");
var state = 0;


//https://stackoverflow.com/questions/1187518/how-to-get-the-difference-between-two-arrays-in-javascript
// Array.prototype.diff = function(a) {
//     return this.filter(function(i) {return a.indexOf(i) < 0;});
// };

//https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color

Reset = "\x1b[0m"
Bright = "\x1b[1m"
Dim = "\x1b[2m"
Underscore = "\x1b[4m"
Blink = "\x1b[5m"
Reverse = "\x1b[7m"
Hidden = "\x1b[8m"

FgBlack = "\x1b[30m"
FgRed = "\x1b[31m"
FgGreen = "\x1b[32m"
FgYellow = "\x1b[33m"
FgBlue = "\x1b[34m"
FgMagenta = "\x1b[35m"
FgCyan = "\x1b[36m"
FgWhite = "\x1b[37m"

BgBlack = "\x1b[40m"
BgRed = "\x1b[41m"
BgGreen = "\x1b[42m"
BgYellow = "\x1b[43m"
BgBlue = "\x1b[44m"
BgMagenta = "\x1b[45m"
BgCyan = "\x1b[46m"
BgWhite = "\x1b[47m"


var kollegen = ["wez"];
console.log(kollegen);
fs.watchFile(
    "./kollegen.json", {
        bigint: false,
        persistent: true,
        interval: 1000
    },
    (curr, prev) => {
        console.log("The file was changed");
        fs.readFile('kollegen.json', (err, data) => {
            if (err) throw err;
            let kollegen_neu = JSON.parse(data);
            console.log(kollegen_neu);
            for (let [key, value] of Object.entries(p)) {
                console.log(`${key}: ${value}`);
            }
        });

    }

);

const username = process.env.USERNAME;
const password = process.env.PASSWORD;
var timetable_data = "";

var kollegen_heute = [];
var kollegen_morgen = [];
var kollegen_heute_changes = [];
var kollegen_morgen_changes = []

var values = [];
var values_morgen = [];
var kuerzel = process.env.KUERZEL;
var factor = 01 * 60; // Abruf alle Minuten
var durchlauf = 0;
//for (const i in kollegen) {
//   console.log("Ausgabe");
//   console.log(kollegen[i]);
//}

var interval = setInterval(function() {
    durchlauf = durchlauf + 1;
    console.log("Hey "+ durchlauf );
    const dsbUntis = new DsbUntis(username, password);
    dsbUntis.fetch().then((data) => {
        if (timetable_data != JSON.stringify(data)) {
            timetable_data = JSON.stringify(data);
            values = [];
            //for (var a in kollegen) {
             //   console.log(kollegen[a]);
            //}

            for (var a in kollegen) {
                kollegen_heute[kollegen[a]] = [];
                kollegen_morgen[kollegen[a]] = [];
               // console.log(kollegen_heute[kollegen[a]]);
               // console.log(kollegen_morgen[kollegen[a]]);
                //console.log(kollegen_morgen);
                for (var i = 0; i < data[0]["table"].length; i++) {
                    if (data[0]["table"][i][0] == kollegen[a] || data[0]["table"][i][5] == kollegen[a]) {
                        kollegen_heute[kollegen[a]].push({
                            'Vertreter': data[0]["table"][i][0],
                            'Stunde': data[0]["table"][i][1],
                            'Fach': data[0]["table"][i][2],
                            'Klasse': data[0]["table"][i][3],
                            'Raum': data[0]["table"][i][4],
                            'statt': data[0]["table"][i][5],
                            'Art': data[0]["table"][i][6],
                            'Text': data[0]["table"][i][7] //,
                        });
                    }
                }
                      //  console.log("Morgen");
                       //    console.log(data[1]["table"]);
                        //console.log(data[1])

                        // console.log(kollegen_morgen);
                        console.log(data[1]["table"])
                        for (var g = 0; g < data[1]["table"].length; g++) {
                            console.log("g");
                            
                            if (data[1]["table"][g][0] == kollegen[a] || data[1]["table"][g][5] == kollegen[a]) {
                                kollegen_morgen[kollegen[a]].push({
                                    'Vertreter': data[1]["table"][g][0],
                                    'Stunde': data[1]["table"][g][1],
                                    'Fach': data[1]["table"][g][2],
                                    'Klasse': data[1]["table"][g][3],
                                    'Raum': data[1]["table"][g][4],
                                    'statt': data[1]["table"][g][5],
                                    'Art': data[1]["table"][g][6],
                                    'Text': data[1]["table"][g][7] //,
                                });
                            }
                        }

                    }

                }
                //for (var z in kollegen) {
                 //   console.log(i);
                  //  console.table(kollegen_heute[kollegen[z]]);
               // }
                console.clear();
                console.log("\n\nHeute");

                for (b in kollegen_heute) {
                    if (kollegen_heute[b].length <= 0) {

                    } else {
                        console.table(kollegen_heute[b]);
                    }
                    
                    //console.log("Kollegen morgen");
                    //console.log(kollegen_morgen);
                }
                console.log("\n\nMorgen");
                
                for (y in kollegen_morgen) {
                    if (kollegen_morgen[y].length <= 0) {

                    } else {
                        console.table(kollegen_morgen[y]);
                    }
                    
                }
                
                kollegen_heute_changes = kollegen_heute;
                kollegen_morgen_changes = kollegen_morgen;
                //console.log(kollegen_morgen_changes);
                console.log('\x1b[36m%s\x1b[0m', 'I am cyan');  //cyan

    });
}, factor * 1000, "Running ...");
